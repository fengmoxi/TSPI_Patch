#!/bin/bash

if [ -z "$1" ] || [ ! -e "$1" ] || [ -z "$2" ] || [ ! -e "$2" ]; then
    echo "Usages: $0 <android_sdk_dir> <via_apk_file>";
    exit 0;
fi

working_dir=`pwd`

# 移除可能存在的冲突文件
rm -rf $working_dir/android_remove_lightning.patch

# 移除闪电浏览器
curl -fsSL https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidViaInsteadLightning/android_remove_lightning.patch -o $working_dir/android_remove_lightning.patch
patch -p1 -N -d $1/vendor/rockchip/common < $working_dir/android_remove_lightning.patch
rm -rf $working_dir/android_remove_lightning.patch

# 将APK写入/system/app
mkdir -p $1/device/rockchip/rk356x/rk3566_tspi/preinstall
echo -e "include \$(call all-subdir-makefiles)\r\n" > $1/device/rockchip/rk356x/rk3566_tspi/preinstall/Android.mk
echo > $1/device/rockchip/rk356x/rk3566_tspi/preinstall/preinstall.mk
echo -e "预置不可卸载apk 将apk文件放置该目录即可 apk最好不带中文\r\n" > $1/device/rockchip/rk356x/rk3566_tspi/preinstall/README.txt
cp -f $2 $1/device/rockchip/rk356x/rk3566_tspi/preinstall/$(md5sum $2 | cut -d' ' -f1).apk