# Android 触摸点亮屏幕

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本补丁用于实现Android系统触摸点亮屏幕。

## 补丁使用教程

当前补丁基于repo版本Android SDK制作：tspi_android_sdk_repo_20240202。

```
# 在Android SDK同级目录下执行

# （可选）将屏幕超时从永不改为1分钟，如需其他时间，修改patch文件中的60000（单位为毫秒）
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidTouchToScreenOn/android_screen_off_timeout_60000.patch
patch -p1 -N -d tspi_android_sdk/device/rockchip/rk356x < android_screen_off_timeout_60000.patch

# 禁用自动休眠
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidTouchToScreenOn/android_disable_auto_suspend.patch
patch -p1 -N -d tspi_android_sdk/frameworks/base < android_disable_auto_suspend.patch
echo -e "\r\nPRODUCT_PROPERTY_OVERRIDES += ro.pms.interactive_mode=true" >> tspi_android_sdk/device/rockchip/rk356x/rk3566_tspi/rk3566_tspi.mk
echo -e "\r\nPRODUCT_PROPERTY_OVERRIDES += ro.pms.auto_suspend=false" >> tspi_android_sdk/device/rockchip/rk356x/rk3566_tspi/rk3566_tspi.mk

# 添加触摸点亮屏幕
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidTouchToScreenOn/android_touch_screen_on.patch
patch -p1 -N -d tspi_android_sdk/frameworks/native < android_touch_screen_on.patch
echo -e "\r\nPRODUCT_PROPERTY_OVERRIDES += persist.sys.touch_screen_on.enable=true" >> tspi_android_sdk/device/rockchip/rk356x/rk3566_tspi/rk3566_tspi.mk
```
