# Android 默认输入法改为讯飞输入法

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本脚本用于实现Android默认输入法由AOSP输入法改为[讯飞输入法](https://srf.xunfei.cn/#/)。

## 补丁使用教程

当前补丁基于repo版本Android SDK制作：tspi_android_sdk_repo_20240202。

```
# 将讯飞输入法apk文件（如iFlyIME_v13.0.7.15091.apk）复制到Android SDK同级目录下
# 在Android SDK同级目录下执行
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidDefaultIMEiFlyIME/android_default_ime_iflyime.sh
chmod a+x android_default_ime_iflyime.sh
./android_default_ime_iflyime.sh tspi_android_sdk iFlyIME_v13.0.7.15091.apk
```

## 拓展

### 改为其他输入法

将android_default_ime_iflyime.patch中的讯飞包名/类名（com.iflytek.inputmethod/.FlyIME）改为对应输入法包名/类名

 - 百度：com.baidu.input/.ImeService
 - 搜狗：com.sohu.inputmethod.sogou/.SogouIME
 - 腾讯：com.tencent.qqpinyin/.QQPYInputMethodService
 - 小艺：com.huawei.ohos.inputmethod/com.android.inputmethod.latin.LatinIME
 - 谷歌：com.google.android.inputmethod.pinyin/.PinyinIME
 - 触宝：com.cootek.smartinput5/.TouchPalIME
