# Android 默认手势导航

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本补丁用于实现Android系统默认使用手势导航而不是三按钮导航

## 补丁使用教程

当前补丁基于repo版本Android SDK制作：tspi_android_sdk_repo_20240202。

```
# 在Android SDK同级目录下执行
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidNavigationBarModeGestural/android_navigation_bar_mode_gestural.sh
chmod a+x android_navigation_bar_mode_gestural.sh
./android_navigation_bar_mode_gestural.sh tspi_android_sdk
```
