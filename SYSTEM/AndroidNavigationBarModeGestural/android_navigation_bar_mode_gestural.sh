#!/bin/bash

if [ -z "$1" ] || [ ! -e "$1" ]; then
    echo "Usages: $0 <android_sdk_dir>";
    exit 0;
fi

# 默认手势导航
echo -e "\r\nPRODUCT_PROPERTY_OVERRIDES += ro.boot.vendor.overlay.theme=com.android.internal.systemui.navbar.gestural" >> $1/device/rockchip/rk356x/rk3566_tspi/rk3566_tspi.mk

# 修复“三按钮”导航高度问题
cat > $1/frameworks/base/packages/overlays/NavigationBarMode3ButtonOverlay/res/values/dimens.xml  << EOF
<?xml version="1.0" encoding="utf-8"?>
<!--
/**
 * Copyright (c) 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
-->
<resources>
    <!-- Height of the bottom navigation / system bar. -->
    <dimen name="navigation_bar_height">48dp</dimen>
    <!-- Height of the bottom navigation bar in portrait; often the same as @dimen/navigation_bar_height -->
    <dimen name="navigation_bar_height_landscape">48dp</dimen>
    <!-- Width of the navigation bar when it is placed vertically on the screen -->
    <dimen name="navigation_bar_width">48dp</dimen>
    <!-- Height of the bottom navigation / system bar. -->
    <dimen name="navigation_bar_frame_height">48dp</dimen>
    <!-- The height of the bottom navigation gesture area. -->
    <dimen name="navigation_bar_gesture_height">48dp</dimen>
</resources>
EOF