#!/bin/bash

if [ -z "$1" ] || [ ! -e "$1" ] || [ -z "$2" ] || [ ! -e "$2" ]; then
    echo "Usages: $0 <android_sdk_dir> <packagename>";
    exit 0;
fi

working_dir=`pwd`

# 移除可能存在的冲突文件
rm -rf $working_dir/android_auto_open_app.patch

# 获取patch文件
curl -fsSL https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidAutoOpenApp/android_auto_open_app.patch -o $working_dir/android_auto_open_app.patch

# 将packagename写入patch
sed -i "s@//xxxxx.xxxxx.xxxxx@//$2@g" android_auto_open_app.patch

# 打补丁
patch -p1 -N -d $1/frameworks/base < $working_dir/android_auto_open_app.patch

rm -rf $working_dir/android_auto_open_app.patch
