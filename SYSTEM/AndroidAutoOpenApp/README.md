# Android 开机自动打开应用

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本脚本用于实现Android开机自动打开无法监听"android.intent.action.BOOT_COMPLETED"的应用（如非系统应用）

## 补丁使用教程

当前补丁基于repo版本Android SDK制作：tspi_android_sdk_repo_20240202。

```
# 在Android SDK同级目录下执行
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidAutoOpenApp/android_auto_open_app.sh
chmod a+x android_auto_open_app.sh
./android_auto_open_app.sh tspi_android_sdk com.xiaomi.smarthome
```

注：可用[预装脚本](TOOL/Android_PreInstall)预装应用。