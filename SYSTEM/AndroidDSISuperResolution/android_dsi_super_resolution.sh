#!/bin/bash

if [ -z "$1" ] || [ ! -e "$1" ] || [ -z "$2" ] || [ -z "$3" ]; then
    echo "Usages: $0 <android_sdk_dir> <display_width> <display_height>";
    exit 0;
fi

mkfile=$1/device/rockchip/rk356x/rk3566_tspi/rk3566_tspi.mk
echo "" >> $mkfile
echo "PRODUCT_PROPERTY_OVERRIDES += vendor.hwc.device.primary=DSI" >> $mkfile
echo "PRODUCT_PROPERTY_OVERRIDES += vendor.hwc.device.extend=HDMI-A,TV" >> $mkfile
echo "PRODUCT_PROPERTY_OVERRIDES += ro.vendor.hdmirotationlock=false" >> $mkfile
echo "PRODUCT_PROPERTY_OVERRIDES += persist.vendor.framebuffer.main="$2"x"$3"@60" >> $mkfile