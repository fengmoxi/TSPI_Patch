# Android DSI超分辨率显示配置

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本补丁用于实现Android在DSI屏下强制显示指定分辨率。

## 补丁使用教程

当前补丁基于repo版本Android SDK制作：tspi_android_sdk_repo_20240202。

```
# 在Android SDK同级目录下执行
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/AndroidDSISuperResolution/android_dsi_super_resolution.sh
chmod a+x android_dsi_super_resolution.sh
./android_dsi_super_resolution.sh tspi_android_sdk 1920 1080
```
