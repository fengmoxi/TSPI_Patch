# 39pin底板USB启用、耳机输入检测补丁

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本补丁用于适配官方39pin底板，修补USB电源启用、耳机输入检测启用、网卡启用

## 注意事项

**由于部分Linux系统未知原因，无法使用网卡功能，本补丁对此情况无效。**

网卡需要配置 Delayline 参数，相关介绍点击[此处](../../GMAC/Auto_RGMII_Delayline)跳转。

## 补丁使用教程

当前补丁基于repo版本SDK制作，
Linux SDK为：tspi_linux_sdk_repo_20240131，
Android SDK为：tspi_android_sdk_repo_20240202。

```
# 在SDK同级目录下执行

# Android SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/EXP/Android_EXP.patch
patch -p1 -N -d tspi_android_sdk/kernel < Android_EXP.patch
# 打自动扫描 delayline 补丁（非必要）
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/GMAC/Auto_RGMII_Delayline/Android_Auto_RGMII_Delayline.patch
patch -p1 -N -d tspi_android_sdk/kernel < Android_Auto_RGMII_Delayline.patch

# Linux SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/SYSTEM/EXP/Linux_EXP.patch
patch -p1 -N -d tspi_linux_sdk/kernel < Linux_EXP.patch
# 打自动扫描 delayline 补丁（非必要）
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/GMAC/Auto_RGMII_Delayline/Linux_Auto_RGMII_Delayline.patch
patch -p1 -N -d tspi_linux_sdk/kernel < Linux_Auto_RGMII_Delayline.patch

```
