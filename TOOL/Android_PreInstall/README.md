# Android 软件预装脚本

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本脚本用于实现Android预装软件。

## 脚本使用教程

当前脚本基于repo版本Android SDK制作：tspi_android_sdk_repo_20240202。

```
# 在Android SDK同级目录下执行

# 预置不可卸载apk
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/TOOL/Android_PreInstall/android_preinstall.sh
chmod a+x android_preinstall.sh
./android_preinstall.sh tspi_android_sdk <apk文件或apk文件所在目录>

# 预置可卸载apk (恢复出厂设置可恢复)
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/TOOL/Android_PreInstall/android_preinstall_del.sh
chmod a+x android_preinstall_del.sh
./android_preinstall_del.sh tspi_android_sdk <apk文件或apk文件所在目录>

# 预置可卸载apk (恢复出厂设置不可恢复)
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/TOOL/Android_PreInstall/android_preinstall_del_forever.sh
chmod a+x android_preinstall_del_forever.sh
./android_preinstall_del_forever.sh tspi_android_sdk <apk文件或apk文件所在目录>
```

## 拓展

### 改为其他浏览器

使用该浏览器的apk即可，无需修改其他内容