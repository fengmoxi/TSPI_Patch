#!/bin/bash

if [ -z "$1" ] || [ ! -e "$1" ] || [ -z "$2" ] || [ ! -d "$2" -a ! -f "$2" ]; then
    echo "Usages: $0 <android_sdk_dir> <apk_dir>";
    exit 0;
fi

mkdir -p $1/device/rockchip/rk356x/rk3566_tspi/preinstall
echo -e "include \$(call all-subdir-makefiles)\r\n" > $1/device/rockchip/rk356x/rk3566_tspi/preinstall/Android.mk
echo > $1/device/rockchip/rk356x/rk3566_tspi/preinstall/preinstall.mk

if [ -f "$2" ]; then
    cp -f $2 $1/device/rockchip/rk356x/rk3566_tspi/preinstall/$(md5sum $2 | cut -d' ' -f1).apk
elif [ -d "$2" ]; then
    for file in $2/*.apk; do
        if [ -f "$file" ]; then
            cp -f "$file" $1/device/rockchip/rk356x/rk3566_tspi/preinstall/$(md5sum "$file" | cut -d' ' -f1).apk
        fi
    done
fi
