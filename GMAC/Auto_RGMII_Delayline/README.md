# 自动扫描Delayline补丁

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

根据瑞芯微官方文档可知，RK3566芯片具有千兆以太网的功能，泰山派使用 RGMII 接口，为了兼容各种不同硬件所带来的信号差异，需要调整 (TX/RX) RGMII delayline 以达到以太网性能最优。
否则可能出现网口获取不到IP（手动填写IP网关等也无法ping通网关），丢包严重等问题。

可通过以下代码扫描合适的 delayline 参数后填入设备树，瑞芯微官方并不建议使用自动扫描功能，并且在SDK中并未提供该功能。
```
echo 1000 > /sys/devices/platform/fe010000.ethernet/phy_lb_scan
```

## 注意事项

**由于部分Linux系统未知原因，无法使用网卡功能，本补丁对此情况无效。**

首次启动系统时会自动扫描并保存合适的 delayline ，之后启动将自动读取保存的 delayline 参数启动。

建议初次启动系统时，不要接入网线以提高自动扫描稳定性和准确性，启动后等待3分钟后重启系统，再接入网线使用网卡功能。

## 补丁使用教程

当前补丁基于repo版本SDK制作，
Linux SDK为：tspi_linux_sdk_repo_20240131，
Android SDK为：tspi_android_sdk_repo_20240202。

```
# 在SDK同级目录下执行

# Android SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/GMAC/Auto_RGMII_Delayline/Android_Auto_RGMII_Delayline.patch
patch -p1 -N -d tspi_android_sdk/kernel < Android_Auto_RGMII_Delayline.patch
# 在设备树中打开GMAC，若已打开无需打本补丁
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/GMAC/Auto_RGMII_Delayline/Android_Enable_GMAC.patch
patch -p1 -N -d tspi_android_sdk/kernel < Android_Enable_GMAC.patch

# Linux SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/GMAC/Auto_RGMII_Delayline/Linux_Auto_RGMII_Delayline.patch
patch -p1 -N -d tspi_linux_sdk/kernel < Linux_Auto_RGMII_Delayline.patch
# 在设备树中打开GMAC，若已打开无需打本补丁
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/GMAC/Auto_RGMII_Delayline/Linux_Enable_GMAC.patch
patch -p1 -N -d tspi_linux_sdk/kernel < Linux_Enable_GMAC.patch
```
