# 立创·泰山派开发板自制补丁合集

欢迎访问立创·泰山派自制补丁合集仓库，本仓库补丁致力于为立创·泰山派官方repo SDK进行功能扩充、Bug修复。

赞美嘉立创，赞美吴工，赞美开发菌~

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 官方资料

### 项目介绍
 - [立创·泰山派开发板2G+16G版本](https://lckfb.com/project/detail/lctspi-2g-16g?param=baseInfo)
 - [立创·泰山派开发板1G+0G版本](https://lckfb.com/project/detail/lctspi-1g-0g?param=baseInfo)

### 项目教程资料
 - [开发板在线资料](https://lceda001.feishu.cn/wiki/IJtRwVu5kiylHykl3RJcQ8ANncY)

### 项目相关案例
 - [官方收录](https://lckfb.com/project/detail/lctspi-1g-0g?param=case)
 - [立创开源广场](https://oshwhub.com/explore?filter=26b12f4862cc4aa8a7a6ff653d9de9ed&page=1)

## 合集目录结构

### 功能扩充

#### 屏幕显示
 - 【泰山派等大】大显3.1寸IPS电容屏（D310T9362V1）：点击[此处](DISPLAY/D310T9362V1)了解详情。
 - 【86盒】大显4寸IPS电容屏（D395C930UV0）：点击[此处](DISPLAY/D395C930UV0+ICN6211)了解详情。
 - 【猫猫屏】思坦德6寸TFT电容屏（STL6.0-1-2-A）：点击[此处](DISPLAY/STL6.0-1-2-A)了解详情。

#### 触摸驱动
 - CST128A驱动（大显FT固件）：点击[此处](TOUCHSCREEN/CST128A-FT_FW)了解详情。

#### 输入模块
 - GPIO按键输入：点击[此处](INPUT/GPIO_KEY)了解详情。
 - 红外遥控输入：点击[此处](INPUT/IR_KEY)了解详情。

#### 有线网卡
 - 自动扫描Delayline补丁：点击[此处](GMAC/Auto_RGMII_Delayline)了解详情。

#### LTE网卡
 · 移远EC200A驱动：点击[此处](LTE/EC200A)了解详情。

#### 官方扩展板
 - 39pin底板USB启用、耳机输入检测补丁：点击[此处](SYSTEM/EXP)了解详情。

#### 定制系统
 - Android TV：点击[此处](SYSTEM/AndroidTV)了解详情。
 - Android DSI超分辨率显示配置：点击[此处](SYSTEM/AndroidDSISuperResolution)了解详情。
 - Android 默认手势导航：点击[此处](SYSTEM/AndroidNavigationBarModeGestural)了解详情。
 - Android 默认输入法改为讯飞输入法：点击[此处](SYSTEM/AndroidDefaultIMEiFlyIME)了解详情。
 - Android 闪电浏览器替换为Via：点击[此处](SYSTEM/AndroidViaInsteadLightning)了解详情。
 - Android 触摸点亮屏幕：点击[此处](SYSTEM/AndroidTouchToScreenOn)了解详情。
 - Android 开机自动打开应用：点击[此处](SYSTEM/AndroidAutoOpenApp)了解详情。

### Bug修复

暂无

### 工具
 - repo diff 补丁文件自动patch脚本：点击[此处](TOOL/REPO_DIFF_PATCH)了解详情。
 - Android 软件预装脚本：点击[此处](TOOL/Android_PreInstall)了解详情。