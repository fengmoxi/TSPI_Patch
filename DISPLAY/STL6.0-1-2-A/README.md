# 【猫猫屏】思坦德6寸TFT电容屏（STL6.0-1-2-A）

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

屏幕型号：STL6.0-1-2-A

触摸型号：GT970

扩展板开源地址：https://oshwhub.com/fengmoxi/taishan-pie-6-inch-screen-adapter-board-moat-version

## 补丁使用教程

当前补丁基于repo版本SDK制作，
Linux SDK为：tspi_linux_sdk_repo_20240131，
Android SDK为：tspi_android_sdk_repo_20240202。

1. 安装dos2unix（已安装可跳过）
```
sudo apt-get update
sudo apt-get install -y dos2unix
```
2. 将SDK中相应文件转换成Unix格式
> patch文件由git管理，换行符为unix格式，repo版SDK设备树文件为dos格式
```
# 在SDK同级目录下执行
# Android SDK
dos2unix tspi_android_sdk/kernel/arch/arm64/boot/dts/rockchip/tspi-rk3566-dsi-v10.dtsi
# Linux SDK
dos2unix tspi_linux_sdk/kernel/arch/arm64/boot/dts/rockchip/tspi-rk3566-dsi-v10.dtsi
```
3. 下载patch文件并为SDK打补丁
```
# 在SDK同级目录下执行
# Android SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/DISPLAY/STL6.0-1-2-A/android-kernel-STL6_0_1_2_A.patch
patch -p1 -N -d tspi_android_sdk/kernel < android-kernel-STL6_0_1_2_A.patch
# Linux SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/DISPLAY/STL6.0-1-2-A/linux-kernel-STL6_0_1_2_A.patch
patch -p1 -N -d tspi_linux_sdk/kernel < linux-kernel-STL6_0_1_2_A.patch
```
