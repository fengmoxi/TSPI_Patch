# 【泰山派等大】大显3.1寸IPS电容屏（D310T9362V1）

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

屏幕型号：D310T9362V1

触摸型号：CST128-A

扩展板开源地址：https://oshwhub.com/fengmoxi/tai-shan-pai-3-1-cun-ping-mu-kuo-zhan-ban

## 扩展板简介

泰山派3.1寸 mipi屏扩展板，通过pogopin连接泰山派底部触点，实现PD供电、串口、RTC、扬声器、麦克风功能。

串口芯片使用CH343，可以看作CH340芯片的技术革新，支持最高6Mbps串口波特率，一般情况下免驱（驱动下载：https://www.wch.cn/products/CH343.html）。

通过一根6pin同向0.5mm间距和一根31pin同向0.3mm间距FFC/FPC软排线与泰山派连接。

RTC电池使用1.25座连接，扬声器使用1.25座连接，YZ-2520 侧发声腔体喇叭。

## 注意事项

淘宝厂家发货的触摸芯片有两种固件可能，一种I2C地址为0x38，另一种I2C地址为0x48（预估为CST原版固件）。购买时建议备注0x38触摸固件。

## 补丁使用教程

当前补丁基于repo版本SDK制作，
Linux SDK为：tspi_linux_sdk_repo_20240131，
Android SDK为：tspi_android_sdk_repo_20240202。

1. 安装dos2unix（已安装可跳过）
```
sudo apt-get update
sudo apt-get install -y dos2unix
```
2. 将SDK中相应文件转换成Unix格式
> patch文件由git管理，换行符为unix格式，repo版SDK设备树文件为dos格式
```
# 在SDK同级目录下执行
# Android SDK
dos2unix tspi_android_sdk/kernel/arch/arm64/boot/dts/rockchip/tspi-rk3566-dsi-v10.dtsi
# Linux SDK
dos2unix tspi_linux_sdk/kernel/arch/arm64/boot/dts/rockchip/tspi-rk3566-dsi-v10.dtsi
```
3. 下载patch文件并为SDK打补丁
```
# 在SDK同级目录下执行
# Android SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/DISPLAY/D310T9362V1/android-kernel-D310T9362V1-0x38.patch
patch -p1 -N -d tspi_android_sdk/kernel < android-kernel-D310T9362V1-0x38.patch
# Linux SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/DISPLAY/D310T9362V1/linux-kernel-D310T9362V1-0x38.patch
patch -p1 -N -d tspi_linux_sdk/kernel < linux-kernel-D310T9362V1-0x38.patch
```
