# 红外遥控

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本补丁使用设备树中写好的红外配置，添加百度影棒盒子、华为盒子、爱奇艺盒子、百视通盒子、当贝盒子五种国内常见遥控器键值。

## 注意事项

红外接收头（IRM-H638T/TR2）OUT引脚接泰山派40pin接口的PWM15_IR_M0。

![电路演示](1.png)

## 补丁使用教程

当前补丁基于repo版本SDK制作，
Linux SDK为：tspi_linux_sdk_repo_20240131，
Android SDK为：tspi_android_sdk_repo_20240202。

```
# 在SDK同级目录下执行

# Android SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/INPUT/IR_KEY/android_irkey.patch
patch -p1 -N -d tspi_android_sdk/kernel < android_irkey.patch

# Linux SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/INPUT/IR_KEY/linux_irkey.patch
patch -p1 -N -d tspi_linux_sdk/kernel < linux_irkey.patch
```
