# 移远EC200A驱动

## 声明

**本项目按其当前的状态开源分享，不带任何明示或暗示的保证。**

**使用本开源项目，需具有相当的电子软硬件等知识，并自行斟酌是否存在任何风险，由于使用本项目造成的人员伤害、物品损坏等后果，作者不承担责任。**

**如果不同意以上信息，请立刻关闭本页面，如果同意请继续阅读。**

## 介绍

本补丁用于驱动移远EC200A模块

## 注意事项

本补丁仅在官方39pin扩展板下连接EC200A模块测试，其他连接方式可能无法使用。

介于Linux根文件系统可能性较多，暂仅提供内核补丁和Android驱动，Linux需要在根文件系统下另行安装相关驱动。

## 补丁使用教程

当前补丁基于repo版本SDK制作，
Android SDK为：tspi_android_sdk_repo_20240202。

```
# 在SDK同级目录下执行

# Android SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/LTE/EC200A/EC200A-Android.sh
chmod a+x EC200A-Android.sh
./EC200A-Android.sh tspi_android_sdk

# Linux SDK
wget https://gitee.com/fengmoxi/TSPI_Patch/raw/master/LTE/EC200A/EC200A-Kernel.patch
patch -p1 -N -d tspi_linux_sdk/kernel < EC200A-Kernel.patch
# 在根文件系统中安装所需驱动
```
